import tkinter as tk
from tkinter import ttk
import json
from Episode import Episode
from Season import Season
from SubtitleFile import SubtitleFile

class SettingsMenu(tk.Menu):
    def __init__(self, master, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self.master = master

        self.loadMenu = tk.Menu(self, tearoff=0)
        self.add_cascade(label="Load", menu=self.loadMenu)

        self.loadMenu.add_command(label="Load Show", command=self.master.selectShowButtonAction)

        self.settingsMenu = tk.Menu(self, tearoff=0)
        self.add_cascade(label="Settings", menu=self.settingsMenu)

        self.settingsMenu.add_command(label="Set Episodes per file", command=self.setEpisodesPerFile)
        self.settingsMenu.add_command(label="Set lowest season", command=self.setSmallestSeasonNumber)
        self.settingsMenu.add_command(label="Set Episodes filetype", command=self.setEpisodesFiletype)
        self.settingsMenu.add_command(label="Set Subtitles filetype", command=self.setSubtitlesFiletype)

        #self.settingsMenu.add_command(label="Set Specials Season", command=self.setSpecialsSeason)

        self.helpMenu = tk.Menu(self, tearoff=0)
        self.add_cascade(label="Help", menu=self.helpMenu)

        self.helpMenu.add_command(label="Keycommands", command=self.showKeycommands)
        self.helpMenu.add_command(label="About", command=self.aboutButtonAction)

    def setSmallestSeasonNumber(self):
        number = tk.simpledialog.askinteger("Smallest season number", "Enter a number:")
        if number is not None and number > 0:
            Season.smallestSeasonNumber = number

    def setEpisodesPerFile(self):
        number = tk.simpledialog.askinteger("Number of Episodes per File", "Enter a number:")
        if number is not None and number > 0:
            Episode.episodesPerFile = number


    def setEpisodesFiletype(self):
        newFiletype = tk.simpledialog.askstring("Filetype of Episodes", "Enter a file extension in format .xxx:")
        if newFiletype is not None:
            if newFiletype[0] == "." :
                Episode.filetype = newFiletype

    def setSubtitlesFiletype(self):
        newFiletype = tk.simpledialog.askstring("Filetype of Subtitles", "Enter a file extension in format .xxxxx:")
        if newFiletype is not None:
            if newFiletype[0] == "." :
                SubtitleFile.filetype = newFiletype

    #def setSpecialsSeason(self):
    #    specialsSeasonName = tk.simpledialog.askstring("Specials Season Name", "Enter the name:")


    def aboutButtonAction(self):
        tk.messagebox.showinfo("About", "This program was created by SBV in 2024\n GPLv3 License")


    def showKeycommands(self):
        keycommanddictionary = json.load(open("keycommands.json"))
        keycommands = ""
        for command, keycommand in keycommanddictionary.items():
            keycommands += keycommand.replace("Control", "Ctrl").replace("<", "").replace(">", "") + " : " + command + "\n"
        tk.messagebox.showinfo("Keycommands lookup", keycommands)

        

        
            