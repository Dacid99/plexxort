import os
from File import File


class Directory(File):

    def __iter__(self):
        return iter(sorted(self._contentsSet))

    def __init__(self, directorypath):
        super().__init__(directorypath)
        self._contentsSet = set() #for unique files
        self._gather()

    def _gather(self):
        for filename in os.listdir(self._path):
            filepath = os.path.join(self._path, filename)
            self.include(filepath)

    def include(self, filepath):
        pass

    def exclude(self, item) :
        if item in self._contentsSet :
            self._contentsSet.remove(item)

    def excludeall(self):
        self._contentsSet.clear()

    def cleanUp(self):
        for item in self._contentsSet:
            if not item.isexistant():
                self.exclude(item)

    def paths(self):
        paths = set()
        for File in self._contentsSet :
            paths.add(File.path())
        return paths

    def namesWithTag(self):
        names = set()
        for item in self._contentsSet :
            names.add(item.nameWithTag())
        return names
    
    def names(self):
        names_notag = set()
        for File in self._contentsSet :
            names_notag.add(File.name())
        return names_notag

    def delete(self):
        #for File in self._contentsSet :  is a problem combined with File.delete done previously
        #    File.delete()   
        self.excludeall()
        super().delete()

    def count(self):
        return len(self._contentsSet)
        newPath = os.path.join(
            newDirectorypath, self.name()
        )
        os.rename(self._path, newPath)
        self._path = newPath
        return newPath
 
    def isempty(self):
        return self.count() == 0

    def areexistant(self):
        exist = True
        for item in self._contentsSet :
            exist = exist and item.isexistant()
        return exist
