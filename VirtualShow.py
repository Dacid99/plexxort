from VirtualDirectory import VirtualDirectory
from VirtualSeason import VirtualSeason


class VirtualShow(VirtualDirectory):

    def _gather(self):
        for Season in self._file:
           self.include(VirtualSeason(Season))

    def virtualNumber(self):
        return "" #so no number is shown in column