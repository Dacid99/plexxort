import os
import shutil

class File :
    def __init__(self, filepath):
        if os.path.exists(filepath):
            self._path = filepath
        else:
            raise ValueError(f"{filepath} does not exist!")

    def __eq__(self, other):
        if isinstance(other, File):
            return self.path() == other.path()
        else:
            raise TypeError(f"Cannot compare File and {type(other)}!\nfrom File.__eq__")
    
    def __lt___(self, other):
        if isinstance(other, File):
            return self.path() < other.path()
        else:
            raise TypeError(f"Cannot compare File and {type(other)}!\nfrom File.__lt__")

    def __hash__(self):
        return id(self)

    def __str__(self):
        return self.name()

    def __int__(self):
        return 0 #placeholder

    def nameWithTag(self):
        return os.path.basename(self._path)

    def name(self):
        return os.path.splitext(self.nameWithTag())[0]

    def tag(self):
        nameparts = os.path.splitext(self.nameWithTag())
        tag = ""
        for tagpart in nameparts[1:]:  #0 element is name, all others tag
            tag += tagpart
        return tag

    def path(self):
        return self._path

    def pathNotag(self):
        return os.path.splitext(self._path)[0]

    def directory(self):
        return os.path.dirname(self._path)

    def size(self):
        return os.path.getsize(self._path)

    def isexistant(self):
        return os.path.exists(self._path)
            
    def delete(self):
        os.remove(self._path)
        self._path = "" #to avoid copying of other file into the scope of this object

    def move(self, newPath):
        os.rename(self._path, newPath)
        self._path = newPath


    #finegrain renaming
    def move2(self, newDirectorypath):
        newFilepath = os.path.join(
            newDirectorypath, self.nameWithTag()
        )
        self.move(newFilepath)
        return newFilepath

    def moveButKeepTag(self, newPath):
        newPathSameTag = os.path.splitext(newPath)[0] + self.tag()
        os.rename(self._path, newPathSameTag)
        self._path = newPathSameTag

    def rename(self, newName):
        newPath = os.path.join(
            self.directory(), newName + self.tag()
        )
        self.move(newPath)
        return newPath

    def retag(self, newTag):
        newPath = os.path.join(
            self.directory(), self.name() + newTag
        )
        self.move(newPath)
        return newPath

    def copy2(self, duplicateDirectorypath):
        duplicatePath = os.path.join(
            duplicateDirectorypath, self.nameWithTag()
        )
        shutil.copy(self._path, duplicatePath)
        return duplicatePath

    def copy_retag(self, copyDirectory, newTag):
        newCopyPath = os.path.join(
            copyDirectory, self.name() + newTag
        )
        copyPath = self.copy2(copyDirectory)
        os.rename(copyPath, newCopyPath)
        return newCopyPath



