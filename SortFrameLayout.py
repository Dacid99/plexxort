import tkinter as tk
from Layout import Layout

class SortFrameLayout(Layout):
    def show(self):
        self._set_windowgrid(5, 5)

        self._window.tree.grid(row=0, column=0, sticky="swen")

        self._window.scrollbar.grid(row=0 , column=1 ,sticky="w")

        self._window.upButton.grid(row= 2, column=1 , sticky="n" )

        self._window.downButton.grid(row=3 , column=1 , sticky="n" )

        self._window.parentUpButton.grid(row=1 , column=1 , sticky="n" )

        self._window.parentDownButton.grid(row=4 , column=1 , sticky="n" )

        self._window.deleteButton.grid(row=4 , column=2 , sticky="n" )

        self._window.changeEpisodeNumberButton.grid(row=2 , column=0 , sticky="n" )

        self._window.toggleSpecialsButton.grid(row=3, column=0, sticky="n")

        self._window.specialEpisodeButton.grid(row=1 , column=2 , sticky="n" )

        self._window.clearButton.grid(row= 1, column=3 , sticky="n" )

        self._window.resetButton.grid(row= 1, column=4 , sticky="n" )

        self._window.enactButton.grid(row= 4, column=4 , sticky="n" )

        self._window.undoButton.grid(row= 2, column=4, sticky="n")

        self._window.renameShowEntry.grid(row=3, column=3, sticky="s")

        self._window.showRenameButton.grid(row=4, column=3, sticky="n")
