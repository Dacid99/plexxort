from VirtualDirectory import VirtualDirectory
from VirtualEpisode import VirtualEpisode
from Season import Season
import os
import re

class VirtualSeason(VirtualDirectory):

    def __eq__(self, other):
        if isinstance(other, VirtualSeason):
            return self.virtualNumber() == other.virtualNumber()
        else:
            raise(TypeError(f"Cannot compare VirtualSeason and {type(other)}!\nfrom VirtualSeason.__eq__"))
    
    def __lt__(self, other):
        if isinstance(other, VirtualSeason):
            return self.virtualNumber() < other.virtualNumber()
        else:
            raise(TypeError(f"Cannot compare VirtualSeason and {type(other)}!\nfrom VirtualSeason.__lt__"))
        
    def __hash__(self):
        return super().__hash__()

    def virtualNumber(self):
        if self._file.isSpecialsSeason():
            return "Specials"
        match = re.search(Season.seasonNumberPattern, self.virtualName())
        if match:
            return int(match.groups()[0])
        else:
            return 0

    def _resetIndicators(self):
        self.renumberIndicator = False
        super()._resetIndicators()


    def _gather(self):
        for Episode in self._file:
           self.include(VirtualEpisode(Episode))


    def realize(self):
        super().realize()
        if self.renumberIndicator:
            self._file.move(self._virtualPath)
            self.renumberIndicator = False


    def renumber(self, newNumber):
        self.renumberIndicator = True
        if self._file.isSpecialsSeason():
            newNumber = 0
        self._virtualPath = os.path.join(
            self.virtualDirectory(), f"Season{newNumber:02}"
        )

    def toggleSpecialsSeason(self):
        self._file.toggleSpecialsSeason()

    def isSpecialsSeason(self):
        self._file.isSpecialsSeason()





