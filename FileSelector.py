import tkinter as tk
from tkinter import filedialog
import os
import logging


class FileSelector:
    def __init__(self, master):
        self.master = master


    def selectedDirectory(self):
        selectedDirectory = filedialog.askdirectory(
                title = "Select the directory containing the show",
                initialdir = os.getcwd(),
                parent = self.master,
                mustexist = True
            )
        return selectedDirectory

          
    """
    def selectFiles(self, Files):
        selectedFilepaths = tk.filedialog.askopenfilenames(
                initialdir = os.getcwd(), 
                title=f"Select {Episode.filetype} files",
                filetypes=[(f"{Episode.filetype} files", Episode.filetype)],
                parent=self.root,
                multiple=True
            )
        for filepath in selectedFilepaths :
            try :
                typefiles.include(filepath)
            except ValueError as e:  #when filepath doesnt exist
                logging.warning(str(e))
    """