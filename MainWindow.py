import tkinter as tk
from tkinter import ttk
import logging
from FileSelector import FileSelector
from Backend import Backend
from SortFrame import SortFrame
from SettingsMenu import SettingsMenu
from MainWindowLayout import MainWindowLayout


class MainWindow(tk.Tk):
    def __init__(self):
        super().__init__()

        self.protocol("WM_DELETE_WINDOW", self.close)

        #Message handling
        logging.basicConfig(
            filename='log.txt',
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)s:%(message)s',
            datefmt='%m/%d/%Y %I:%M:%S %p'
        ) #this should pass through to all other components

        #file handling
        self.fileselector = FileSelector(self)

        #Backend
        self._backend = Backend(self.fileselector)

        #SortFrame
        self.sortFrame = SortFrame(self, self._backend)
        
        #button for settings
        self.menu = SettingsMenu(self)
        self.config(menu = self.menu)
        
        #keybindings
        self.bind("<Control-o>", self.selectShowButtonAction)

        #layout   
        self.layout = MainWindowLayout(self)

        #start program
        self.activate()


    #decorator to control frame activity
    def toggleFrame(func):
        def wrapper(self,*args,**kwargs):
            result = func(self,*args, **kwargs)
            if self._backend.isready():
                self.sortFrame.enable()
            else:
                self.sortFrame.disable()

            return result
        return wrapper


    @toggleFrame
    def selectShowButtonAction(self):
        self._backend.selectShow()
        if self._backend.virtualShow:
            self.sortFrame.insertShow(self._backend.virtualShow)

    @toggleFrame
    def reset(self):
        self._backend.reset()
        

    def activate(self):
        logging.info("----------------------------------------\nStart")
        self.mainloop()
    
        
    def close(self):
        logging.info("End\n----------------------------------------")
        self.destroy()

