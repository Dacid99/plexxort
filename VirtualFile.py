import os
import logging
from File import File

class VirtualFile:
    def __init__(self, file):
        if not isinstance(file, File):
            logging.error("VirtualFile was passed a non-File object!\nfrom VirtualFile.__init__")
            raise TypeError("VirtualFile was passed a non-File object!\nfrom VirtualFile.__init__")
        self._file = file
        self._reset()


    def withCheck(func):
        def wrapper(self, *args, **kwargs): 
            if self._file is None:
                raise(RuntimeError("VirtualFile is not assigned to any File!"))
            func(self, *args, **kwargs)
        return wrapper


    def __eq__(self, other):
        if isinstance(other, VirtualFile):
            return self.virtualPath() == other.virtualPath()
        else:
            raise(TypeError(f"Cannot compare VirtualFile and {type(other)}!\nfrom VirtualFile.__eq__"))


    def __lt__(self, other):
        if isinstance(other, VirtualFile):
            return self.virtualPath() < other.virtualPath()
        else:
            raise(TypeError(f"Cannot compare VirtualFile and {type(other)}!\nfrom VirtualFile.__lt__"))


    def __hash__(self):
        return id(self)


    def __str__(self):
        return self.virtualName()

    def __int__(self):
        return int(self._file)

    def _reset(self): 
        self._virtualPath = self._file.path()
        self._resetIndicators()


    def virtualDirectory(self):
        return os.path.dirname(self._virtualPath)

    def virtualPath(self):
        return self._virtualPath

    def virtualNameWithTag(self):
        return os.path.basename(self._virtualPath)

    def virtualTag(self):
        return os.path.splitext(self.virtualNameWithTag())[1]

    def virtualName(self):
        return os.path.splitext(self.virtualNameWithTag())[0]


    def _resetIndicators(self):
        self.moveIndicator = False   #moving to other directory, preserving name
        self.renameIndicator = False #preserve directory, change name, KEEP EXT
        self.retagIndicator = False  #preserve directory and name, change ext
        self.removeIndicator = False #delete


    def virtualRemove(self):
        self.removeIndicator = True


    def virtualMove2(self, newDirectory):
        self.moveIndicator = True
        self._virtualPath = os.path.join(
            newDirectory, self.virtualNameWithTag()
        )

    def virtualRename(self, newName):
        self.renameIndicator = True
        self._virtualPath = os.path.join(
            self.virtualDirectory(), newName + self.virtualTag()
        )

    def virtualRetag(self, newTag):
        self.retagIndicator = True
        self._virtualPath = os.path.join(
            self.virtualDirectory(), self.name() + newTag
        )

    #@withCheck
    def realize(self):
        if self.removeIndicator:
            self._file.delete()
            self.removeIndicator = False
            return
        if self.renameIndicator or self.retagIndicator or self.moveIndicator:
            self._file.move(self._virtualPath)
            self.renameIndicator = self.moveIndicator = self.retagIndicator = False


    def undoChanges(self):
        self._reset()
    


