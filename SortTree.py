import tkinter as tk
from tkinter import ttk
from bidict import bidict
import logging
from VirtualEpisode import VirtualEpisode
from VirtualSeason import VirtualSeason
from VirtualShow import VirtualShow
from Episode import Episode
from Season import Season

class SortTree(ttk.Treeview):
    def __init__(self, master, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self._itemObjectMap = bidict()
        self._itemObjectMap[""] = ""  #set root object as it always exists

        self.configure(columns = ("name", "number"))
        self.heading("name", text = "Name")
        self.heading("number", text = "Number")

    def selected(self):
        selectedObjects = []
        for item in self.selection():
            selectedObjects.append( self._getObjectFromItem(item) )
        return selectedObjects

    def _getItemFromObject(self, object):
        return self._itemObjectMap.inverse.get(object)

    def _getObjectFromItem(self, itemID):
        return self._itemObjectMap.get(itemID)

    def insertObject(self, parentObject, object):
        itemID = str(id(object))
        parentID = self._getItemFromObject(parentObject)
        self.insert(parentID, "end", iid=itemID, values= (str(object), object.virtualNumber()), open = True)
        self._itemObjectMap[itemID] = object

    def updateItem(self, item):
        object = self._getObjectFromItem(item)
        self.item(item, values = (str(object), object.virtualNumber()))

    def updateObject(self, object):
        item = self._getItemFromObject(object)
        self.item(item, values = (str(object), object.virtualNumber()))

    def fullUpdate(self):
        for item in self._itemObjectMap.keys():
            self.updateItem(item)


    def delete(self, item):
        self._itemObjectMap.pop(item)
        itemChildren = self.get_children(item)
        for child in itemChildren:
            self._itemObjectMap.pop(child)
            itemGrandchildren = self.get_children(child)
            for grandchild in itemGrandchildren:
                self._itemObjectMap.pop(grandchild) #as deep as the hierarchy can go for this program
        super().delete(item)

    def getItems(self):
        return set(self._itemObjectMap.keys())

    def moveSelectionUp(self):
        for selectedItem in self.selection() :
            parent = self.parent(selectedItem)
            if parent:
                index = self.index(selectedItem)
                newIndex = (index - 1) % len(self.get_children(parent))
                self.move(selectedItem, parent, newIndex)
            else : 
                continue
        logging.debug("moveSelectionUp")


    def moveSelectionDown(self):
        for selectedItem in self.selection() :
            parent = self.parent(selectedItem)
            if parent:
                index = self.index(selectedItem)
                newIndex = (index + 1) % len(self.get_children(parent))
                self.move(selectedItem, parent, newIndex)
            else:
                continue
        logging.debug("moveSelectionDown")


    def moveSelectionParentUp(self):
        for selectedItem in self.selection() :
            parent = self.parent(selectedItem)
            parentParent = self.parent(parent)
            if parent and parentParent:
                parentIndex = self.index(parent)
                newParentIndex = (parentIndex - 1) % len(self.get_children(parentParent))
                newParent = self.get_children(parentParent)[newParentIndex]
                self.move(selectedItem, newParent, "end")
            else:
                logging.debug("parent or parentParent is false")
                logging.debug(parent)
                logging.debug(parentParent)
                continue
        logging.debug("moveSelectionParentUp")


    def moveSelectionParentDown(self):
        for selectedItem in self.selection() :
            parent = self.parent(selectedItem)
            parentParent = self.parent(parent)
            if parent and parentParent:
                parentIndex = self.index(parent)
                newParentIndex = (parentIndex + 1) % len(self.get_children(parentParent))
                newParent = self.get_children(parentParent)[newParentIndex]
                self.move(selectedItem, newParent, "0")
            else:
                logging.debug("parent or parentParent is false")
                logging.debug(parent)
                logging.debug(parentParent)
                continue
        logging.debug("moveSelectionParentDown")


    def move2parent(self, parentObject):
        parentID = self._getItemFromObject(parentObject)
        for selectedItem in self.selection() :
            self.move(selectedItem, parentID, "end")


    def deleteSelection(self):
        for selectedItem in self.selection() :
            self.delete(selectedItem)


    def objectsIndex(self, object):
        item = self._getItemFromObject(object)
        return self.index(item)


    def objectsParentObject(self, object):
        item = self._getItemFromObject(object)
        parent = self.parent(item)
        return self._getObjectFromItem(parent)


    def numberObjects(self):
        for item in self.get_children():
            object = self._getObjectFromItem(item)
            if not isinstance(object, VirtualShow):
                print("wrong order in tree!")
                logging.error("wrong order in tree!")
            else:
                for subitem in self.get_children(item):
                    subobject = self._getObjectFromItem(subitem)
                    if not isinstance(subobject, VirtualSeason):
                        print("wrong order in tree!")
                        logging.error("wrong order in tree!")
                    else:
                        seasonNumber = self.index(subitem) + Season.smallestSeasonNumber
                        if subobject.isSpecialsSeason():
                            seasonNumber = 0
                        subobject.renumber(seasonNumber)
                        self.updateItem(subitem)

                        print(f"renumbering episodes in {seasonNumber}")
                        sumOfEpisodeNumbers = 0
                        for subsubitem in self.get_children(subitem):
                            subsubobject = self._getObjectFromItem(subsubitem)
                            if not isinstance(subsubobject, VirtualEpisode):
                                print("wrong order in tree!")
                                logging.error("wrong order in tree!")
                            else:
                                print(self.index(subsubitem))
                                episodeNumbers = [sumOfEpisodeNumbers + i+1 for i in range(0, subsubobject.getEpisodesPerFile())] 
                                subsubobject.renumber(seasonNumber, episodeNumbers)
                                self.updateItem(subsubitem)
                                sumOfEpisodeNumbers += subsubobject.getEpisodesPerFile()
                                
        logging.debug("numberObjects")


    def clear(self):
        parentItems = self.get_children()
        for item in parentItems:
            self.delete(item)

        for item in self._itemObjectMap.items():
            print(item)
        


