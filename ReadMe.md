# Plexxort
=========================

A tkinter GUI for sorting episode and season files for Plex.

Most of the time episodes are not arranged the way movieDB or tvDB know them. They may be in wrong order, or double episodes in one file etc. 
This GUI helps with sorting them correctly so Plex can find the correct files.

*This is experimental, there are still bugs. ALWAYS MAKE A BACKUP BEFORE USING IT!*