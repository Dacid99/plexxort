import tkinter as tk
import logging
from VirtualShow import VirtualShow
from Show import Show

class Backend:
    def __init__(self, fileselector):
        self.fileselector = fileselector
        self.virtualShow = None
        self.specialsSeason = None
        
    def selectShow(self):
        showDirectory = self.fileselector.selectedDirectory()
        if showDirectory:
            logging.debug("Show" + showDirectory + "selected")
            self.virtualShow = VirtualShow(Show(showDirectory))
            logging.debug("Show" + showDirectory + "indexed")
        else:
            logging.debug("No show selected")


    def isready(self):
        logging.debug("Backend ready check")
        return self.virtualShow is not None
        

    def reset(self):
        self.virtualShow = None

