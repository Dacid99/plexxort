import os
from Season import Season
from Directory import Directory


class Show(Directory):
    def include(self, filepath): #only directories
        if os.path.isdir(filepath) : #no duplicates ensured by set, __eq__ and __hash__
            newSeason = Season(filepath)
            if newSeason.number() is None:
                fallbackNumber = ( self.size() + 1 )
                newSeason.changeNumber(fallbackNumber)
            self._contentsSet.add( newSeason )


    def realize(self):
        #order is crucial
        #first episodes then seasons IF seasons are reorderable
        for Season in self._contentsSet :
            Season.realize()
        self.rename()

