from Episode import Episode
from VirtualFile import VirtualFile
import re

class VirtualEpisode(VirtualFile):
    def __init__(self, Episode):
        super().__init__(Episode)
        self._reset()

    def __eq__(self, other):
        if isinstance(other, VirtualEpisode):
            return self.virtualNumber() == other.virtualNumber()
        else:
            raise(TypeError(f"Cannot compare VirtualEpisode and {type(other)}!\nfrom VirtualEpisode.__eq__"))
    
    def __lt__(self, other):
        if isinstance(other, VirtualEpisode):
            return self.virtualNumber() < other.virtualNumber()
        else:
            raise(TypeError(f"Cannot compare VirtualEpisode and {type(other)}!\nfrom VirtualEpisode.__lt__"))
        

    def __hash__(self):
        return super().__hash__()


    def realize(self):
        #order is crucial
        #numbering  moving  renaming
        if self.renumberIndicator:
            self._file.move(self._virtualPath)
            self.renumberIndicator = False
        super().realize()


    def _reset(self):
        self._virtualNumber = self._file.number()
        super()._reset()


    def _resetIndicators(self):
        self.renumberIndicator = False  #change number in name, preserving basic name and directory
        super()._resetIndicators()


    def setEpisodesPerFile(self, newEpisodesPerFileNumber):
        self._file.setEpisodesPerFile( newEpisodesPerFileNumber )

    def getEpisodesPerFile(self):
        return self._file.episodesPerFile

    def virtualNumber(self):
        return self._virtualNumber


    def renumber(self, newSeasonNumber, newEpisodeNumber):
        if len(newEpisodeNumber) != self.getEpisodesPerFile():
            raise ValueError("newEpisodeNumber is shorter than episodesPerFile!\nfromVirtualEpisode.renumber")
        #if len(newSeasonNumber) != 1:
        #    raise ValueError("newSeasonNumber is of wrong length not 1!\nfromVirtualEpisode.renumber")
        
        def replacement(match):
            newBlock = f"S{newSeasonNumber:02}"
            for number in newEpisodeNumber:
                newBlock += f"E{number:02}_"
            newBlock = newBlock.removesuffix('_') #remove last overshooting underscore

            if match.group(1) and not match.group(1).endswith("-"):
                newBlock = "-" + newBlock
            if match.group(3) and not match.group(3).startswith("-"):
                newBlock = newBlock + "-"

            replacement = f"{match.group(1)}{newBlock}{match.group(3)}"
            return replacement
    
        newName = re.sub(Episode.episodeNamePattern, replacement, self.virtualName())  #potential issues with double occurence of pattern
        print(newName)
    
        self._virtualNumber = newEpisodeNumber
        self.renumberIndicator = True
        self.virtualRename(newName)
            






