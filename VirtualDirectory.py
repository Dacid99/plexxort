import os
import logging
from VirtualFile import VirtualFile
from Directory import Directory


class VirtualDirectory(VirtualFile):

    def __iter__(self):
        return iter(sorted(self._contentsSet))

    def __init__(self, directory):
        if not isinstance(directory, Directory):
            logging.error("VirtualDirectory was passed a non-Directory object!\nfrom VirtualDirectory.__init__")
            raise TypeError("VirtualDirectory was passed a non-Directory object!\nfrom VirtualDirectory.__init__")
        super().__init__(directory)
        self._contentsSet = set()
        self._gather()

    def realize(self):
        for item in self._contentsSet:
            item.realize()
        super().realize()
        
    def _gather(self):
        pass

    def virtualRemove(self):
        for item in self._contentsSet:
            item.virtualRemove()
        super().virtualRemove()

    def include(self, item):
        for i in self._contentsSet:
            print("before", i, hash(i))
        self._contentsSet.add(item)
        for i in self._contentsSet:
            print("after", i, hash(i))

    def exclude(self, item):
        for i in self._contentsSet:
            print("before remove", i, hash(i))
        print("item", item, hash(item))
        print(item in self._contentsSet)
        self._contentsSet.remove(item)

    def cleanUp(self):
        #cleanUp in wrapped directory
        self._file.cleanUp()
        #then read directory again
        self._contentsSet = set()
        self._gather()

    def undoChanges(self):
        for item in self._contentsSet :
            item.undoChanges()
        self._reset()