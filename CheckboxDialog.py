import tkinter as tk
from tkinter import ttk
import json

class CheckboxDialog(tk.simpledialog.Dialog):
    def __init__(self, parent, optionsfilename, title):
        self.checkboxes = []
        self.optionsVariables = {}
        with open(optionsfilename) as file:
            self.options = json.load(file)

        super().__init__(parent, title)


    def body(self, master):
        for optionDisplayName, option in self.options.items():
            self.optionsVariables[option]= tk.BooleanVar()
            self.checkboxes.append(ttk.Checkbutton(master, text=optionDisplayName, variable = self.optionsVariables[option]))
        
        for checkbox in self.checkboxes:
            checkbox.pack()

        return self.checkboxes[0]


    def apply(self):
        self.result = []
        for option, optionsVariable in self.optionsVariables.items():
            if optionsVariable.get():
                self.result.append(option)

