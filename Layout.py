import tkinter as tk
from tkinter import ttk
import numpy as np
from screeninfo import get_monitors

class Layout:
    def __init__(self, window):
        self._window = window
        self.show()
        

    @staticmethod
    def _get_screen_dimensions():
        monitors = get_monitors()
        screen_width, screen_height = monitors[0].width, monitors[0].height
        return screen_width, screen_height


    def _set_size_relative_to_screen(self, width_ratio, height_ratio):
        screen_width, screen_height = Layout._get_screen_dimensions()
        initial_width = int(screen_width * width_ratio)
        initial_height = int(screen_height * height_ratio)
        self._window.geometry(f"{initial_width}x{initial_height}")


    def _set_windowgrid(self, rownumber, columnnumber):
        windowgrid = { "rows" : np.arange(0, rownumber),
                       "columns" : np.arange(0, columnnumber) }

        for rowindex in windowgrid["rows"] :
            self._window.rowconfigure(rowindex, weight = 1)
        
        for columnindex in windowgrid["columns"] :
            self._window.columnconfigure(columnindex, weight = 1)


    def show(self):
        pass
