from Directory import Directory
from Episode import Episode
import re

class Season(Directory):

    seasonNumberPattern = r'(\d+)'
    smallestSeasonNumber = 1 #default

    def __init__(self, directorypath):
        super().__init__(directorypath)
        self.isSpecialsSeasonBool = False

    def __eq__(self, other):
        if isinstance(other, Season):
            return self.number() == other.number()
        else:
            raise TypeError(f"Cannot compare Season and {type(other)}!\nfrom Season.__eq__")
    
    def __lt__(self, other):
        if isinstance(other, Season):
            return self.number() < other.number()
        else:
            raise TypeError(f"Cannot compare Season and {type(other)}!\nfrom Season.__lt__")

    def __hash__(self):
        return super().__hash__()

    def __int__(self):
        return self.number()

    def include(self, filepath): #only Episodes
        if filepath.endswith(Episode.fileType): #no duplicates ensured by set, __eq__ and __hash
            newEpisode = Episode(filepath)
            self._contentsSet.add( newEpisode )

    def number(self):
        if self.isSpecialsSeasonBool:
            return "Specials"
        match = re.search(Season.seasonNumberPattern, self.name())
        if match:
            return int(match.groups()[0])
        else:
            return 0

    def toggleSpecialsSeason(self):
        self.isSpecialsSeasonBool = not self.isSpecialsSeasonBool

    def isSpecialsSeason(self):
        return self.isSpecialsSeasonBool


    """ name will always be replaced to Season \d+
    def seasonName(self):
        seasonName = self.name()
        while re.sub(Season.episodeBlockNumberPattern, "", episodeName) is not episodeName :
            continue

        return episodeName
    """
              