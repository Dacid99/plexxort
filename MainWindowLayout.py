from Layout import Layout


class MainWindowLayout(Layout):
    def show(self):
        self._window.title("Plexxort Application")
        self._set_size_relative_to_screen(0.55,0.5)
        self._set_windowgrid(1,1)
        self._window.sortFrame.grid(row=1,column=1, sticky = "nsew")
        
