import tkinter as tk
from tkinter import ttk
import json
import logging
import re
from SortTree import SortTree
from SortFrameLayout import SortFrameLayout
from VirtualShow import VirtualShow
from VirtualEpisode import VirtualEpisode
from VirtualSeason import VirtualSeason


class SortFrame(tk.Frame):
    def __init__(self, master, backend, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self.backend = backend

        self.tree = SortTree(self, height = 20, selectmode = "extended")
        self.scrollbar = tk.Scrollbar(self, orient=tk.VERTICAL, command=self.tree.yview)

        self.tree.config(yscrollcommand=self.scrollbar.set)

        self.upButton = ttk.Button(self, text="\u2191", command=self.upButtonAction)

        self.downButton = ttk.Button(self, text="\u2193", command=self.downButtonAction)

        self.parentUpButton = ttk.Button(self, text="\u21D1", command=self.parentUpButtonAction)

        self.parentDownButton = ttk.Button(self, text="\u21D3", command=self.parentDownButtonAction)

        self.deleteButton = ttk.Button(self, text="Delete", command= self.deleteButtonAction)

        self.changeEpisodeNumberButton = ttk.Button(self, text="Change number of episodes", command= self.changeEpisodeNumberButtonAction)

        self.toggleSpecialsButton = ttk.Button(self, text="Set as specials season", command= self.toggleSpecialsButtonAction)

        self.specialEpisodeButton = ttk.Button(self, text = "Move to specials season", command= self.specialEpisodeButtonAction)

        self.clearButton = ttk.Button(self, text="Clear", command= self.clearButtonAction)

        self.resetButton = ttk.Button(self, text="Reset", command= self.resetButtonAction)

        self.enactButton = ttk.Button(self, text="Enact changes", command= self.enactButtonAction)

        self.undoButton = ttk.Button(self, text="Undo", command=self.undoButtonAction)

        self.renameShowEntry = ttk.Entry(self)

        self.showRenameButton = ttk.Button(self, text="Rename Show", command= self.showRenameButtonAction)

        self.bindKeys()

        self.layout= SortFrameLayout(self)
        #disabled by default 
        #self.disable()


    def withBackup(func):
        def wrapper(self,*args,**kwargs):
            self.backupTree = self.tree
            func(self,*args,**kwargs)
        return wrapper

    
    def disable(self):  #implement later, more complex than expected
        pass
        #for widget in self.winfo_children():
        #    widget.config(state=tk.DISABLED)
    
    def enable(self):
        pass
        #for widget in self.winfo_children():
        #    widget.config(state=tk.NORMAL)
    

    def insertShow(self, virtualShow):
        if not isinstance(virtualShow, VirtualShow) :
            logging.error("Input is not a VirtualShow!")
            raise TypeError("Input is not a VirtualShow!")

        self.tree.insertObject("", virtualShow)
        for virtualSeason in virtualShow:
            self.tree.insertObject(virtualShow, virtualSeason)
            for virtualEpisode in virtualSeason:
                self.tree.insertObject(virtualSeason, virtualEpisode)

        self.renameShowEntry.delete(0,"end")
        self.renameShowEntry.insert(0, str(virtualShow))

        logging.debug("insertShow")


    def deleteButtonAction(self):
        for selectedObject in self.tree.selected():
            selectedObject.virtualRemove()
        self.tree.deleteSelection()


    @withBackup
    def upButtonAction(self):
        self.tree.moveSelectionUp()
        logging.debug("upButtonAction")
        

    @withBackup
    def downButtonAction(self):
        self.tree.moveSelectionDown()
        logging.debug("downButtonAction")
        

    def parentUpButtonAction(self):
        for selectedObject in self.tree.selected():
            if selectedObject:
                parentSelectedObject = self.tree.objectsParentObject(selectedObject)
                if parentSelectedObject: #catches show in selection, parent will be ""
                    parentSelectedObject.exclude(selectedObject)
                
        #first move up a parent in tree
        self.tree.moveSelectionParentUp()
        #then move to the new parent in files

        for selectedObject in self.tree.selected():
            if selectedObject:
                newParentSelectedObject = self.tree.objectsParentObject(selectedObject)
                if newParentSelectedObject:
                    newParentSelectedObject.include(selectedObject)
                    selectedObject.virtualMove2(newParentSelectedObject.virtualPath())
                
        logging.debug("parentUpButtonAction")


    def parentDownButtonAction(self):
        for selectedObject in self.tree.selected():
            if selectedObject:
                parentSelectedObject = self.tree.objectsParentObject(selectedObject)
                if parentSelectedObject:
                    parentSelectedObject.exclude(selectedObject)
            
        #first move down a parent in tree
        self.tree.moveSelectionParentDown()
        #then move to the new parent in files

        for selectedObject in self.tree.selected():
            if selectedObject:
                newParentSelectedObject = self.tree.objectsParentObject(selectedObject)
                if newParentSelectedObject:
                    newParentSelectedObject.include(selectedObject)
                    selectedObject.virtualMove2(newParentSelectedObject.virtualPath())
                
        logging.debug("parentDownButtonAction")

    def changeEpisodeNumberButtonAction(self):
        for selectedObject in self.tree.selected():
            if selectedObject:
                if isinstance(selectedObject, VirtualEpisode):
                    number = tk.simpledialog.askinteger("Number of Episodes per File", "How many episodes are in this file?")
                    if number is not None and number >= 0:
                        selectedObject.setEpisodesPerFile(number)
                        self.tree.updateObject(selectedObject)

    def toggleSpecialsButtonAction(self):
        for selectedObject in self.tree.selected():
            if isinstance(selectedObject, VirtualSeason):
                selectedObject.toggleSpecialsSeason()
                self.tree.updateObject(selectedObject)
                if self.backend.specialsSeason:
                    self.backend.specialsSeason.toggleSpecialsSeason() #untoggle old specialsseason
                self.backend.specialsSeason = selectedObject #set new specialsseason
                return #only change one season in selection

    def specialEpisodeButtonAction(self):
        for selectedObject in self.tree.selected():
            if selectedObject:
                parentSelectedObject = self.tree.objectsParentObject(selectedObject)
                if parentSelectedObject: #catches show in selection, parent will be ""
                    parentSelectedObject.exclude(selectedObject)
                
        #first move up a parent in tree
        self.tree.move2parent(self.backend.specialsSeason)
        #then move to the new parent in files

        for selectedObject in self.tree.selected():
            if selectedObject:
                newParentSelectedObject = self.tree.objectsParentObject(selectedObject)
                print(newParentSelectedObject)
                if newParentSelectedObject:
                    newParentSelectedObject.include(selectedObject)
                    selectedObject.virtualMove2(newParentSelectedObject.virtualPath())
                
        logging.debug("specialEpisodeButtonAction")
       

    def clearButtonAction(self):
        if tk.messagebox.askyesno("Are you sure you want to clear?"):
            self.tree.clear()
            self.master.reset()
        logging.debug("clearButtonAction")


    def resetButtonAction(self):
        if tk.messagebox.askyesno("Are you sure you want to reset your changes?"):
            self.tree.clear()
            self.backend.virtualShow.undoChanges()
            self.insertShow(self.backend.virtualShow)
        logging.debug("resetButtonAction")


    def enactButtonAction(self):
        if tk.messagebox.askyesno("Are you sure you want to enact your changes?"):
            print("answered yes")
            self.tree.numberObjects()
            print("objects numbered")
            self.backend.virtualShow.realize()
            print("realized!")
            #need to reset the tree?
        logging.debug("enactChangesButtonAction")

    
    def showRenameButtonAction(self):
        newShowName = self.renameShowEntry.get()
        if not re.search( r'[/\\]', newShowName ):
            self.backend.virtualShow.virtualRename(newShowName)
            self.tree.updateObject(self.backend.virtualShow)
        else:
            tk.messagebox.showinfo("New name must not include / or \!")

        logging.debug("renameShowButtonAction")
     

    def undoButtonAction(self):
        self.tree = self.backupTree
        #show if necessary


    def bindKeys(self):
        self.tree.bind("<Control-k>", lambda event: self.upButtonAction())
        self.tree.bind("<Control-j>", lambda event: self.downButtonAction())
        self.tree.bind("<Control-l>", lambda event: self.parentUpButtonAction())
        self.tree.bind("<Control-h>", lambda event: self.parentDownButtonAction())
        self.tree.bind("<Delete>", lambda event: self.deleteButtonAction())
        #self.tree.bind("<Control-0>", lambda event: self.specialEpisodeButtonAction())
        self.tree.bind("<Control-r>", lambda event: self.resetButtonAction())
        self.tree.bind("<Control-Enter>", lambda event: self.enactButtonAction())
        self.tree.bind("<Control-Delete>", lambda event: self.clearButtonAction())
        self.tree.bind("<Control-z>", lambda event: self.undoButtonAction())