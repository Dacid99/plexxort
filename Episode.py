from File import File
from SubtitleFile import SubtitleFile
import re
import os 

class Episode(File):

    episodesPerFile = 1  # default
    episodeNumberPattern = r'[eE](\d+)'
    episodeSeasonNumberPattern = r'[sS](\d+)'
    episodeBlockNumberPattern = r'[sS]\d+([eE]\d+_?)+'
    episodeNamePattern = r'^(.*?-?\s*)([sS]\d+(?:[eE]\d+_?)+)(\s*-?.*?)$'
    fileType = ".mkv"  # default
   

    def __init__(self, filepath):
        super().__init__(filepath)

        self.subtitleFile = None
        subtitleFilepath = self.pathNotag() + SubtitleFile.filetype
        if os.path.exists(subtitleFilepath):
            print(subtitleFilepath)
            self.subtitleFile = SubtitleFile(subtitleFilepath)

        self.episodesPerFile = Episode.episodesPerFile #set to default on construction

    def __eq__(self, other):
        if isinstance(other, Episode):
            return self.number() == other.number()
        else:
            raise TypeError(f"Cannot compare Episode and {type(other)}!\nfrom Episode.__eq__")
    
    def __lt__(self, other):
        if isinstance(other, Episode):
            return self.number() < other.number()
        else:
            raise TypeError(f"Cannot compare Episode and {type(other)}!\nfrom Episode.__lt__")

    def __hash__(self): 
        return super().__hash__()

    def __int__(self):
        return self.number()

    def number(self):
        #find block with season and episode number
        bigMatch = re.search(Episode.episodeBlockNumberPattern, self.name())

        #find episode numbers in block
        if bigMatch:
            match = re.findall(Episode.episodeNumberPattern, bigMatch.group())
            #no if-else match  required, as no match impossible, see structure of block regex
            if (difference := Episode.episodesPerFile - len(match)) <= 0:
                return list(map(int, match[-Episode.episodesPerFile:]))  #take last numbers
            elif difference > 0:
                return list(map(int, match + [0]*(difference)))  #patch with 0 to allow comparison

        else:
            return [0]*Episode.episodesPerFile #default to allow comparison


    def episodeName(self):
        episodeName = self.name()
        while re.sub(Episode.episodeBlockNumberPattern, "", episodeName) is not episodeName : #wont work, re.sub must return !
            continue

        return episodeName
    
    def setEpisodesPerFile(self, newEpisodesPerFileNumber):
        self.episodesPerFile = newEpisodesPerFileNumber

    def delete(self):
        super().delete()
        if self.subtitleFile:
            self.subtitleFile.delete()

    def move(self, newPath):
        super().move(newPath)
        if self.subtitleFile:
            self.subtitleFile.moveButKeepTag(newPath)

    """
    def numberName(self, seasonNumber):
        newFileName = self.filename() + "%02d"%(seasonNumber) + "%02d"%(seasonNumber)
        newFilePath = os.path.join(self.directory(), newFileName)
        self.move2(newFilePath)
   

    def realize(self):
        self.numberName()
    """

